<?php

namespace Drupal\page_manager_migration\Utility;

/**
 * Utilities for Page Manager and Panels migrations.
 */
class PmMigrationUtility {

  /**
   * Unserializable properties of panels pane configs in Drupal 7 (DB).
   *
   * @const string
   */
  const PANELS_PANE_UNSERIALIZABLES = [
    'access',
    'configuration',
    'cache',
    'style',
    'css',
    'extras',
    'locks',
  ];

  /**
   * Returns unserialized panels pane configuration.
   *
   * @param array $raw_properties
   *   The discovered raw properties of a panels pane.
   *
   * @return array
   *   The unserialized panels pane configuration.
   */
  public static function unserializePaneSource(array $raw_properties): array {
    foreach ($raw_properties as $prop => $prop_value) {
      if (!in_array($prop, self::PANELS_PANE_UNSERIALIZABLES)) {
        continue;
      }

      try {
        $unserialized = unserialize($prop_value);
        $unserialized_properties[$prop] = $unserialized;
      }
      catch (\Throwable $t) {
      }
    }

    return $unserialized_properties + $raw_properties;
  }

}
