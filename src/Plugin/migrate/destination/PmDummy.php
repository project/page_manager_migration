<?php

declare(strict_types = 1);

namespace Drupal\page_manager_migration\Plugin\migrate\destination;

use Drupal\migrate\Plugin\migrate\destination\DestinationBase;
use Drupal\migrate\Row;

/**
 * A dummy destination plugin for simplifying Panels Pane plugin ID lookup.
 *
 * @MigrateDestination(
 *   id = "pm_dummy",
 *   requirements_met = true,
 *   provider = "core"
 * )
 */
class PmDummy extends DestinationBase {

  /**
   * {@inheritdoc}
   */
  protected $supportsRollback = TRUE;

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['value']['type'] = 'string';
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return ['value' => 'A dummy string value'];
  }

  /**
   * {@inheritdoc}
   */
  public function import(Row $row, array $old_destination_id_values = []) {
    return ['value' => $row->getDestinationProperty('value')];
  }

}
