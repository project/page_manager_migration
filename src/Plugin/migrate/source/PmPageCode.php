<?php

declare(strict_types = 1);

namespace Drupal\page_manager_migration\Plugin\migrate\source;

use Drupal\migrate\Plugin\migrate\source\DummyQueryTrait;

/**
 * Source plugin for code-stored, default Page Manager pages.
 *
 * @MigrateSource(
 *   id = "pm_page_code",
 *   source_module = "page_manager"
 * )
 */
class PmPageCode extends PmPage {

  use DummyQueryTrait;

  /**
   * The known code-provided page manager tasks, and their module dependency.
   *
   * @todo Add defaults to all migratable tasks.
   *
   * @const string[]
   */
  const KNOWN_CODE_TASKS = [
    'blog' => ['module' => 'blog'],
    'blog_user' => ['module' => 'blog'],
    'comment_reply' => ['module' => 'comment'],
    'contact_site' => ['module' => 'contact'],
    'contact_user' => ['module' => 'contact'],
    'node_edit' => ['module' => 'node'],
    'node_view' => [
      'module' => 'node',
      'admin_description' => 'When enabled, this overrides the default Drupal behavior for displaying nodes at <em>/node/{node}</em>. If you add variants, you may use selection criteria such as node type or language or user access to provide different views of nodes. If no variant is selected, the default Drupal node view will be used. This page only affects nodes viewed as pages, it will not affect nodes viewed in lists or at other locations.',
      'path' => '/node/%node',
      'access' => [],
      'arguments' => [
        [
          'keyword' => 'node',
          'identifier' => 'Node being viewed',
          'id' => 1,
          'name' => 'entity_id:node',
          'settings' => [],
        ],
      ],
    ],
    'poll' => ['module' => 'poll'],
    'search' => ['module' => 'search'],
    'term_view' => ['module' => 'taxonomy'],
    'user_edit' => ['module' => 'user'],
    'user_view' => ['module' => 'user'],
  ];

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    foreach (static::KNOWN_CODE_TASKS as $task_id => $data) {
      if (
        !$this->moduleExists($data['module']) ||
        $this->variableGet("page_manager_{$task_id}_disabled", TRUE)
      ) {
        continue;
      }
      $code_tasks[$task_id] = [
        'name' => $task_id,
        'admin_label' => ucfirst(
          implode(' ',
            explode('_', $task_id)
          )
        ),
      ] + $data;
    }
    return new \ArrayIterator($code_tasks);
  }

  /**
   * {@inheritdoc}
   */
  protected function doCount() {
    return (int) $this->initializeIterator()->count();
  }

}
