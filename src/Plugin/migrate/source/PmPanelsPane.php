<?php

declare(strict_types = 1);

namespace Drupal\page_manager_migration\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;
use Drupal\page_manager_migration\Utility\PmMigrationUtility;

/**
 * Source plugin for Panels panes.
 *
 * @MigrateSource(
 *   id = "pm_panels_pane",
 *   source_module = "panels"
 * )
 */
class PmPanelsPane extends DrupalSqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('panels_pane', 'pp')
      ->fields('pp');
    if ($custom_only = !empty($this->configuration['custom_only'])) {
      $query->condition('pp.type', 'custom');
    }
    $query->orderBy('pp.pid');
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row): bool {
    foreach (PmMigrationUtility::unserializePaneSource($row->getSource()) as $prop => $val) {
      $row->setSourceProperty($prop, $val);
    }
    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'pid' => 'Id of the panels pane',
      'did' => 'ID of the panels display',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'pid' => ['type' => 'integer'],
      'did' => ['type' => 'integer'],
    ];
  }

}
