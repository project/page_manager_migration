<?php

declare(strict_types = 1);

namespace Drupal\page_manager_migration\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Source plugin for Page Manager pages stored in database.
 *
 * @MigrateSource(
 *   id = "pm_page",
 *   source_module = "page_manager"
 * )
 */
class PmPage extends DrupalSqlBase {

  /**
   * The name of the 'thing' we migrate.
   *
   * @var string
   */
  protected $thingName = 'page';

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('page_manager_pages', 'pmp')
      ->fields('pmp')
      ->orderBy('pmp.pid');

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    foreach (['access', 'menu', 'arguments', 'conf'] as $serialized_prop) {
      if ($row->hasSourceProperty($serialized_prop)) {
        try {
          $unserialized_value = unserialize($row->getSourceProperty($serialized_prop));
          $row->setSourceProperty($serialized_prop, $unserialized_value);
        }
        catch (\Throwable $t) {

        }
      }
    }
    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return ['name' => "Machine name of the {$this->thingName}"];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return ['name' => ['type' => 'string']];
  }

}
