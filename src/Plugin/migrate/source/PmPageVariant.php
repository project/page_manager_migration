<?php

declare(strict_types = 1);

namespace Drupal\page_manager_migration\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\page_manager_migration\Utility\PmMigrationUtility;

/**
 * Source plugin for Page Manager page variants.
 *
 * @MigrateSource(
 *   id = "pm_page_variant",
 *   source_module = "page_manager"
 * )
 */
class PmPageVariant extends PmPage {

  /**
   * {@inheritdoc}
   */
  protected $thingName = 'page variant';

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('page_manager_handlers', 'pmh')
      ->fields('pmh')
      ->orderBy('pmh.did');

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // Try to get displays and panes for Panels variants.
    if (
      $row->getSourceProperty('handler') === 'panel_context' &&
      $display = $this->getPanelsDisplay($row)
    ) {
      $row->setSourceProperty('panels_display', $display);
      // We have to use 'did' from the display, not from the actual variant row.
      $row->setSourceProperty('panels_panes', $this->getPanelsDisplayPanes($display['did']));
    }

    // Variants might have a weight override.
    $weight = $this->select('page_manager_weights', 'pmw')
      ->condition('name', $row->getSourceProperty('name'))
      ->fields('pmw', ['weight'])
      ->execute()
      ->fetchField();

    if ($weight !== FALSE) {
      $row->setSourceProperty('weight', $weight);
    }

    return TRUE;
  }

  /**
   * Returns the panels display configuration of the actual page variant row.
   *
   * @param \Drupal\migrate\Row $row
   *   The row object.
   *
   * @return array|null
   *   The panels display configuration of the actual page variant row, or NULL
   *   if there aren't any data (because this is not a panels variant).
   */
  protected function getPanelsDisplay(Row $row): ?array {
    if (!$this->getDatabase()->schema()->tableExists('panels_display')) {
      return NULL;
    }

    $displays = $this->select('panels_display', 'pd')
      ->fields('pd')
      ->condition('pd.storage_id', $row->getSourceProperty('name'))
      ->execute()
      ->fetchAll();
    $serialized_props = [
      'layout_settings',
      'panel_settings',
      'cache',
    ];

    foreach ($displays as $delta => $display) {
      foreach ($display as $prop => $prop_value) {
        if (!in_array($prop, $serialized_props)) {
          continue;
        }

        try {
          $unserialized = unserialize($prop_value);
          $displays[$delta][$prop] = $unserialized;
        }
        catch (\Throwable $t) {
        }
      }
    }

    return $displays[0] ?? NULL;
  }

  /**
   * Returns the panels panes' configuration of the given panels page variant.
   *
   * @param string $did
   *   The  display ID of the panels display whose panes we want to get.
   *
   * @return array|null
   *   The panels panes' configuration of the given panels page variant, or NULL
   *   if there aren't any data (because this is not a panels variant).
   */
  protected function getPanelsDisplayPanes(string $did): ?array {
    if (!$this->getDatabase()->schema()->tableExists('panels_pane')) {
      return NULL;
    }

    $panes = $this->select('panels_pane', 'pp')
      ->fields('pp')
      ->condition('pp.did', $did)
      ->execute()
      ->fetchAll();

    foreach ($panes as $delta => $pane) {
      foreach (PmMigrationUtility::unserializePaneSource($pane) as $prop => $val) {
        $panes[$delta][$prop] = $val;
      }
    }

    return $panes;
  }

}
