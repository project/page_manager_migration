<?php

declare(strict_types = 1);

namespace Drupal\page_manager_migration\Plugin\migrate\process;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\MigrateLookupInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Process plugin for page manager (variant) selection criteria.
 *
 * @MigrateProcessPlugin(
 *   id = "pm_page_variant_selection_criteria"
 * )
 */
class PmPageVariantSelectionCriteria extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The migrate lookup service.
   *
   * @var \Drupal\migrate\MigrateLookupInterface
   */
  protected $migrateLookup;

  /**
   * Constructs a new PmPageVariantSelectionCriteria instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\migrate\MigrateLookupInterface $migrate_lookup
   *   The migrate lookup service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrateLookupInterface $migrate_lookup) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->migrateLookup = $migrate_lookup;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('migrate.lookup')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (!$value || !is_array($value)) {
      return NULL;
    }

    if (!($condition_plugin_id = $this->getConditionPluginId($value))) {
      return NULL;
    }

    return array_filter([
      'id' => $condition_plugin_id,
      'context_mapping' => $this->getConditionContextMapping($value),
      'roles' => $this->getConditionRoles($value),
      'bundles' => $this->getConditionBundles($value),
      'negate' => $value['not'] ?? FALSE,
    ]);
  }

  /**
   * Returns the condition plugin ID of the actual Drupal 7 condition.
   *
   * @param array $d7_conf
   *   The Drupal 7 condition configuration.
   *
   * @return string|null
   *   The condition plugin ID of the condition.
   */
  protected function getConditionPluginId(array $d7_conf): ?string {
    switch ($d7_conf['name']) {
      case 'role':
        return 'user_role';

      case 'entity_bundle:node':
        return 'entity_bundle:node';
    }

    return NULL;
  }

  /**
   * Returns the context mapping of the actual Drupal 7 condition.
   *
   * @param array $d7_conf
   *   The Drupal 7 condition configuration.
   *
   * @return array|null
   *   The context mapping of the condition.
   */
  protected function getConditionContextMapping(array $d7_conf): ?array {
    if (empty($d7_conf['context'])) {
      return NULL;
    }

    switch ($d7_conf['context']) {
      case 'logged-in-user':
        return ['user' => '@user.current_user_context:current_user'];
    }

    if (strpos($d7_conf['context'], ':')) {
      $base = explode(':', $d7_conf['context'], 2)[0];
      $derivative = explode(':', $d7_conf['context'], 2)[1];
      switch ($base) {
        case 'argument_entity_id':
          if (preg_match('/^node_\d*$/', $derivative)) {
            return ['node' => 'node'];
          }
          break;
      }
    }

    return NULL;
  }

  /**
   * Returns the user roles configuration of the actual Drupal 7 condition.
   *
   * @param array $d7_conf
   *   The Drupal 7 condition configuration.
   *
   * @return array|null
   *   The user roles of the condition.
   */
  protected function getConditionRoles(array $d7_conf): ?array {
    if (empty($d7_conf['settings']['rids'])) {
      return NULL;
    }

    foreach ($d7_conf['settings']['rids'] as $role_id) {
      if ($lookup_result = $this->migrateLookup->lookup(['d7_user_role'], [$role_id])) {
        $roles[$lookup_result[0]['id']] = $lookup_result[0]['id'];
      }
    }
    return $roles ?? [];
  }

  /**
   * Returns the user roles configuration of the actual Drupal 7 condition.
   *
   * @param array $d7_conf
   *   The Drupal 7 condition configuration.
   *
   * @return array|null
   *   The user roles of the condition.
   */
  protected function getConditionBundles(array $d7_conf): ?array {
    if (empty($d7_conf['settings']['type'])) {
      return NULL;
    }

    foreach ($d7_conf['settings']['type'] as $node_type) {
      if ($lookup_result = $this->migrateLookup->lookup(['d7_node_type'], [$node_type])) {
        $node_types[$lookup_result[0]['type']] = $lookup_result[0]['type'];
      }
    }
    return $node_types ?? [];
  }

}
