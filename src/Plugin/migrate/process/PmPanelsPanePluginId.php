<?php

declare(strict_types = 1);

namespace Drupal\page_manager_migration\Plugin\migrate\process;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\MigrateLookupInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Process plugin which returns the (block) plugin ID of the given Panels pane.
 *
 * @MigrateProcessPlugin(
 *   id = "pm_panels_pane_plugin_id",
 *   handle_multiples = TRUE
 * )
 */
class PmPanelsPanePluginId extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The migrate lookup service.
   *
   * @var \Drupal\migrate\MigrateLookupInterface
   */
  protected $migrateLookup;

  /**
   * Constructs a new PmPanelsPanePluginId instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\migrate\MigrateLookupInterface $migrate_lookup
   *   The migrate lookup service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrateLookupInterface $migrate_lookup) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->migrateLookup = $migrate_lookup;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('migrate.lookup')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $value = $row->getSource();
    $type = $value['type'] ?? NULL;
    $subtype = $value['subtype'] ?? NULL;

    if (
      $type === 'block' &&
      preg_match('/^system-(.*)/', $subtype, $matches)
    ) {
      // This might be a menu block?
      if ($lookup_result = $this->migrateLookup->lookup(['d7_menu'], [$matches[1]])) {
        return 'system_menu_block:' . $lookup_result[0]['id'];
      }
    }

    return NULL;
  }

}
