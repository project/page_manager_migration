<?php

declare(strict_types = 1);

namespace Drupal\page_manager_migration\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Process plugin for page manager parameters.
 *
 * @MigrateProcessPlugin(
 *   id = "pm_page_parameters",
 *   handle_multiples = TRUE
 * )
 */
class PmPageParameters extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $arguments = [];
    foreach ($value as $d7_conf) {
      $arguments[$d7_conf['keyword']] = [
        'machine_name' => $d7_conf['keyword'],
        'label' => $d7_conf['identifier'],
        'type' => $this->getParameterType($d7_conf),
      ];
    }

    return $arguments;
  }

  /**
   * Returns the context plugin ID that matches the given Drupal 7 arg config.
   *
   * @param array $d7_argument_conf
   *   The Drupal 7 page argument config.
   *
   * @return string
   *   The equivalent context plugin ID.
   */
  protected function getParameterType(array $d7_argument_conf): string {
    $base_id = explode(':', $d7_argument_conf['name'])[0];
    $derivative_id = explode(':', $d7_argument_conf['name'], 2)[1] ?? NULL;

    switch ($base_id) {
      case 'entity_id':
        return "entity" . static::DERIVATIVE_SEPARATOR . $derivative_id;
    }

    return '';
  }

}
