<?php

declare(strict_types = 1);

namespace Drupal\page_manager_migration\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Process plugin for page manager page paths.
 *
 * @MigrateProcessPlugin(
 *   id = "pm_page_path"
 * )
 */
class PmPagePath extends ProcessPluginBase {

  /**
   * Regular expression for identifying Drupal 7 arg placeholders.
   *
   * @const string
   */
  const PATH_ARG_REGEXP = '/(?<=\W)%([\w]*)\b/';

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    // We have to convert argument placeholders, so
    // 'node/%node/revision/%revision' should be changed to
    // '/node/{node}/revision/{revision}'.
    $new_path = preg_replace(self::PATH_ARG_REGEXP, '{$1}', $value);
    return '/' . trim($new_path, '/');
  }

}
