<?php
// phpcs:ignoreFile
/**
 * @file
 * A database agnostic dump for testing purposes.
 *
 * This file was generated by the Drupal 9.3.4 db-tools.php script.
 */

use Drupal\Core\Database\Database;

$connection = Database::getConnection();
// Ensure any tables with a serial column with a value of 0 are created as
// expected.
if ($connection->databaseType() === 'mysql') {
  $sql_mode = $connection->query("SELECT @@sql_mode;")->fetchField();
  $connection->query("SET sql_mode = '$sql_mode,NO_AUTO_VALUE_ON_ZERO'");
}

$connection->schema()->createTable('page_manager_pages', array(
  'fields' => array(
    'pid' => array(
      'type' => 'serial',
      'not null' => TRUE,
      'size' => 'normal',
    ),
    'name' => array(
      'type' => 'varchar',
      'not null' => FALSE,
      'length' => '255',
    ),
    'task' => array(
      'type' => 'varchar',
      'not null' => FALSE,
      'length' => '64',
      'default' => 'page',
    ),
    'admin_title' => array(
      'type' => 'varchar',
      'not null' => FALSE,
      'length' => '255',
    ),
    'admin_description' => array(
      'type' => 'text',
      'not null' => FALSE,
      'size' => 'big',
    ),
    'path' => array(
      'type' => 'varchar',
      'not null' => FALSE,
      'length' => '255',
    ),
    'access' => array(
      'type' => 'text',
      'not null' => TRUE,
      'size' => 'big',
    ),
    'menu' => array(
      'type' => 'text',
      'not null' => TRUE,
      'size' => 'big',
    ),
    'arguments' => array(
      'type' => 'text',
      'not null' => TRUE,
      'size' => 'big',
    ),
    'conf' => array(
      'type' => 'text',
      'not null' => TRUE,
      'size' => 'big',
    ),
  ),
  'primary key' => array(
    'pid',
  ),
  'unique keys' => array(
    'name' => array(
      'name',
    ),
  ),
  'indexes' => array(
    'task' => array(
      'task',
    ),
  ),
  'mysql_character_set' => 'utf8',
));

$connection->insert('page_manager_pages')
->fields(array(
  'pid',
  'name',
  'task',
  'admin_title',
  'admin_description',
  'path',
  'access',
  'menu',
  'arguments',
  'conf',
))
->values(array(
  'pid' => '1',
  'name' => 'test_page_1',
  'task' => 'page',
  'admin_title' => 'Test page 1',
  'admin_description' => '',
  'path' => 'test/page/1',
  'access' => 'a:0:{}',
  'menu' => 'a:0:{}',
  'arguments' => 'a:0:{}',
  'conf' => 'a:1:{s:11:"admin_paths";b:0;}',
))
->values(array(
  'pid' => '2',
  'name' => 'simple_landing_page',
  'task' => 'page',
  'admin_title' => 'Simple landing page',
  'admin_description' => 'A simple landing page.',
  'path' => 'landing/1',
  'access' => 'a:0:{}',
  'menu' => 'a:5:{s:4:"type";s:6:"normal";s:5:"title";s:21:"Simple landing page 1";s:4:"name";s:10:"navigation";s:6:"weight";s:1:"1";s:6:"parent";a:4:{s:4:"type";s:4:"none";s:5:"title";s:0:"";s:4:"name";s:10:"navigation";s:6:"weight";s:1:"0";}}',
  'arguments' => 'a:0:{}',
  'conf' => 'a:0:{}',
))
->execute();

// Reset the SQL mode.
if ($connection->databaseType() === 'mysql') {
  $connection->query("SET sql_mode = '$sql_mode'");
}