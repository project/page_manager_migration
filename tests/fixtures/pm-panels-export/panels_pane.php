<?php
// phpcs:ignoreFile
/**
 * @file
 * A database agnostic dump for testing purposes.
 *
 * This file was generated by the Drupal 9.3.4 db-tools.php script.
 */

use Drupal\Core\Database\Database;

$connection = Database::getConnection();
// Ensure any tables with a serial column with a value of 0 are created as
// expected.
if ($connection->databaseType() === 'mysql') {
  $sql_mode = $connection->query("SELECT @@sql_mode;")->fetchField();
  $connection->query("SET sql_mode = '$sql_mode,NO_AUTO_VALUE_ON_ZERO'");
}

$connection->schema()->createTable('panels_pane', array(
  'fields' => array(
    'pid' => array(
      'type' => 'serial',
      'not null' => TRUE,
      'size' => 'normal',
    ),
    'did' => array(
      'type' => 'int',
      'not null' => TRUE,
      'size' => 'normal',
      'default' => '0',
    ),
    'panel' => array(
      'type' => 'varchar',
      'not null' => FALSE,
      'length' => '255',
      'default' => '',
    ),
    'type' => array(
      'type' => 'varchar',
      'not null' => FALSE,
      'length' => '255',
      'default' => '',
    ),
    'subtype' => array(
      'type' => 'varchar',
      'not null' => FALSE,
      'length' => '255',
      'default' => '',
    ),
    'shown' => array(
      'type' => 'int',
      'not null' => FALSE,
      'size' => 'tiny',
      'default' => '1',
    ),
    'access' => array(
      'type' => 'text',
      'not null' => FALSE,
      'size' => 'big',
    ),
    'configuration' => array(
      'type' => 'text',
      'not null' => FALSE,
      'size' => 'big',
    ),
    'cache' => array(
      'type' => 'text',
      'not null' => FALSE,
      'size' => 'big',
    ),
    'style' => array(
      'type' => 'text',
      'not null' => FALSE,
      'size' => 'big',
    ),
    'css' => array(
      'type' => 'text',
      'not null' => FALSE,
      'size' => 'big',
    ),
    'extras' => array(
      'type' => 'text',
      'not null' => FALSE,
      'size' => 'big',
    ),
    'position' => array(
      'type' => 'int',
      'not null' => FALSE,
      'size' => 'small',
      'default' => '0',
    ),
    'locks' => array(
      'type' => 'text',
      'not null' => FALSE,
      'size' => 'big',
    ),
    'uuid' => array(
      'type' => 'char',
      'not null' => FALSE,
      'length' => '36',
    ),
  ),
  'primary key' => array(
    'pid',
  ),
  'indexes' => array(
    'did_idx' => array(
      'did',
    ),
  ),
  'mysql_character_set' => 'utf8',
));

$connection->insert('panels_pane')
->fields(array(
  'pid',
  'did',
  'panel',
  'type',
  'subtype',
  'shown',
  'access',
  'configuration',
  'cache',
  'style',
  'css',
  'extras',
  'position',
  'locks',
  'uuid',
))
->values(array(
  'pid' => '1',
  'did' => '1',
  'panel' => 'top',
  'type' => 'custom',
  'subtype' => 'custom',
  'shown' => '1',
  'access' => 'a:0:{}',
  'configuration' => 'a:6:{s:11:"admin_title";s:20:"Custom pane lp1, top";s:5:"title";s:7:"Pane #1";s:13:"title_heading";s:2:"h2";s:4:"body";s:24:"Pane body in top region.";s:6:"format";s:10:"plain_text";s:10:"substitute";b:1;}',
  'cache' => 'a:0:{}',
  'style' => 'a:1:{s:8:"settings";N;}',
  'css' => 'a:0:{}',
  'extras' => 'a:0:{}',
  'position' => '0',
  'locks' => 'a:0:{}',
  'uuid' => '19917547-01c0-4886-a138-8a56520c9830',
))
->values(array(
  'pid' => '2',
  'did' => '1',
  'panel' => 'bottom',
  'type' => 'custom',
  'subtype' => 'custom',
  'shown' => '1',
  'access' => 'a:0:{}',
  'configuration' => 'a:6:{s:11:"admin_title";s:23:"Custom pane lp1, bottom";s:5:"title";s:7:"Pane #2";s:13:"title_heading";s:2:"h3";s:4:"body";s:19:"Pane 2 body, bottom";s:6:"format";s:10:"plain_text";s:10:"substitute";b:1;}',
  'cache' => 'a:0:{}',
  'style' => 'a:1:{s:8:"settings";N;}',
  'css' => 'a:0:{}',
  'extras' => 'a:0:{}',
  'position' => '0',
  'locks' => 'a:0:{}',
  'uuid' => '0368c41a-a250-41e6-87bc-4adef0b2f84e',
))
->values(array(
  'pid' => '4',
  'did' => '1',
  'panel' => 'left',
  'type' => 'block',
  'subtype' => 'system-main-menu',
  'shown' => '1',
  'access' => 'a:0:{}',
  'configuration' => 'a:3:{s:14:"override_title";i:0;s:19:"override_title_text";s:0:"";s:22:"override_title_heading";s:2:"h2";}',
  'cache' => 'a:0:{}',
  'style' => 'a:1:{s:8:"settings";N;}',
  'css' => 'a:0:{}',
  'extras' => 'a:0:{}',
  'position' => '0',
  'locks' => 'a:0:{}',
  'uuid' => '23df847b-2809-4db0-813a-59547c9e41fa',
))
->values(array(
  'pid' => '6',
  'did' => '1',
  'panel' => 'right',
  'type' => 'block',
  'subtype' => 'user-login',
  'shown' => '1',
  'access' => 'a:0:{}',
  'configuration' => 'a:3:{s:14:"override_title";i:0;s:19:"override_title_text";s:0:"";s:22:"override_title_heading";s:2:"h2";}',
  'cache' => 'a:0:{}',
  'style' => 'a:1:{s:8:"settings";N;}',
  'css' => 'a:0:{}',
  'extras' => 'a:0:{}',
  'position' => '0',
  'locks' => 'a:0:{}',
  'uuid' => '51dc2014-a67a-4275-b394-4ffbed73da16',
))
->values(array(
  'pid' => '7',
  'did' => '1',
  'panel' => 'right',
  'type' => 'block',
  'subtype' => 'system-user-menu',
  'shown' => '1',
  'access' => 'a:1:{s:7:"plugins";a:1:{i:0;a:4:{s:4:"name";s:4:"role";s:8:"settings";a:1:{s:4:"rids";a:1:{i:0;i:1;}}s:7:"context";s:14:"logged-in-user";s:3:"not";b:1;}}}',
  'configuration' => 'a:3:{s:14:"override_title";i:0;s:19:"override_title_text";s:0:"";s:22:"override_title_heading";s:2:"h2";}',
  'cache' => 'a:0:{}',
  'style' => 'a:1:{s:8:"settings";N;}',
  'css' => 'a:0:{}',
  'extras' => 'a:0:{}',
  'position' => '1',
  'locks' => 'a:0:{}',
  'uuid' => 'f7702500-a46c-4823-a4ca-3e1833b30459',
))
->values(array(
  'pid' => '8',
  'did' => '1',
  'panel' => 'middle',
  'type' => 'node',
  'subtype' => 'node',
  'shown' => '1',
  'access' => 'a:0:{}',
  'configuration' => 'a:9:{s:3:"nid";s:1:"2";s:5:"links";i:0;s:16:"leave_node_title";i:0;s:10:"identifier";s:0:"";s:10:"build_mode";s:4:"full";s:15:"link_node_title";i:0;s:14:"override_title";i:0;s:19:"override_title_text";s:0:"";s:22:"override_title_heading";s:2:"h2";}',
  'cache' => 'a:0:{}',
  'style' => 'a:1:{s:8:"settings";N;}',
  'css' => 'a:0:{}',
  'extras' => 'a:0:{}',
  'position' => '0',
  'locks' => 'a:0:{}',
  'uuid' => '8ea0fe88-c68b-4849-ae9f-2ba88f642e8e',
))
->execute();

// Reset the SQL mode.
if ($connection->databaseType() === 'mysql') {
  $connection->query("SET sql_mode = '$sql_mode'");
}