<?php

namespace Drupal\Tests\page_manager_migration\Kernel;

use Drupal\Tests\migmag\Traits\MigMagKernelTestDxTrait;
use Drupal\Tests\migrate_drupal\Kernel\MigrateDrupalTestBase;

/**
 * Tests Page Manager Migration without Panels or PM being installed.
 *
 * @group page_manager_migration
 */
class PmMigrateWithoutPmTest extends MigrateDrupalTestBase {

  use MigMagKernelTestDxTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'filter',
    'image',
    'menu_ui',
    'migmag_process',
    'node',
    'page_manager_migration',
    'path',
    'path_alias',
    'text',
    'block_content',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('block_content');
    $this->installConfig(['node', 'block_content']);

    $this->loadFixture(implode(DIRECTORY_SEPARATOR, [
      dirname(__DIR__, 2),
      'fixtures',
      'pm-panels-export.php',
    ]));
  }

  /**
   * Tests Page Manager and Panels migrations integration with core migrations.
   */
  public function testPmPanelsMigrationsIntegration(): void {
    $this->startCollectingMessages();
    $this->executeMigrations($this->getOrderedDrupalMigrationIds());
    $this->assertNoMigrationMessages();
  }

}
