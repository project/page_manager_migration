<?php

namespace Drupal\Tests\page_manager_migration\Kernel;

use Drupal\Tests\page_manager_migration\Traits\PmMigrateAssertionTrait;

/**
 * Tests Page Manager + Panels migrations (now mainly for debugging purposes).
 *
 * @group page_manager_migration
 */
class PmMigrateTest extends PmMigrateWithoutPmTest {

  use PmMigrateAssertionTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'ctools',
    'layout_discovery',
    'page_manager',
    'panels',
  ];

  /**
   * Tests Page Manager and Panels migrations with only the crucial migrations.
   */
  public function testPmPanelsMigrations(): void {
    $this->startCollectingMessages();
    $this->executeMigrations([
      'd7_user_role',
      'd7_node_type',
      'block_content_type',
      'd7_filter_format',
      'block_content_body_field',
      'd7_user_role',
      'd7_menu',
    ]);

    $this->startCollectingMessages();
    $this->executeMigrations([
      'pm_page',
      'pm_page_code',
      'pm_panels_pane_custom',
      'pm_panels_pane',
      'pm_page_variant',
    ]);
    $this->assertNoMigrationMessages();

    $this->checkPmMigrations();
  }

  /**
   * {@inheritdoc}
   */
  public function testPmPanelsMigrationsIntegration(): void {
    parent::testPmPanelsMigrationsIntegration();

    $this->checkPmMigrations();
  }

  /**
   * Checks the result of the migrations.
   */
  protected function checkPmMigrations() {
    $this->assertSimpleLandingPage();
    $this->assertTestPage1();
  }

}
