<?php

namespace Drupal\Tests\page_manager_migration\Traits;

use Drupal\page_manager\Entity\Page;
use Drupal\page_manager\Entity\PageVariant;

/**
 * Trait for testing migrated pages and page variants.
 */
trait PmMigrateAssertionTrait {

  /**
   * Checks the simple landing page built with panels.
   */
  protected function assertSimpleLandingPage() {
    // Check the page.
    $this->assertEquals(
      [
        'langcode' => 'en',
        'status' => TRUE,
        'id' => 'simple_landing_page',
        'label' => 'Simple landing page',
        'description' => 'A simple landing page.',
        'use_admin_theme' => FALSE,
        'path' => '/landing/1',
        'access_logic' => 'and',
        'access_conditions' => [],
        'parameters' => [],
      ],
      array_diff_key(
        Page::load('simple_landing_page')->toArray(),
        ['uuid' => 1, 'dependencies' => 1]
      )
    );

    // Check the only variant.
    $this->assertEquals(
      [
        'uuid' => '174c4b3d-1451-4b00-8ab4-2f67f31c4b8b',
        'langcode' => 'en',
        'status' => TRUE,
        'id' => 'page_simple_landing_page__panel_context_dabd57ca-648c-4561-be3a-1c6a379910a3',
        'label' => 'Landing page',
        'variant' => 'panels_variant',
        'variant_settings' => [
          'id' => 'panels_variant',
          'label' => 'Landing page',
          'weight' => 0,
          'uuid' => '174c4b3d-1451-4b00-8ab4-2f67f31c4b8b',
          'blocks' => [
            '19917547-01c0-4886-a138-8a56520c9830' => [
              'id' => 'block_content:19917547-01c0-4886-a138-8a56520c9830',
              'label' => 'Pane #1',
              'label_display' => 'visible',
              'provider' => 'block_content',
              'status' => TRUE,
              'info' => '',
              'view_mode' => 'full',
              'region' => 'top',
              'weight' => 0,
              'uuid' => '19917547-01c0-4886-a138-8a56520c9830',
            ],
            '0368c41a-a250-41e6-87bc-4adef0b2f84e' => [
              'id' => 'block_content:0368c41a-a250-41e6-87bc-4adef0b2f84e',
              'label' => 'Pane #2',
              'label_display' => 'visible',
              'provider' => 'block_content',
              'status' => TRUE,
              'info' => '',
              'view_mode' => 'full',
              'region' => 'bottom',
              'weight' => 0,
              'uuid' => '0368c41a-a250-41e6-87bc-4adef0b2f84e',
            ],
            '23df847b-2809-4db0-813a-59547c9e41fa' => [
              'id' => 'system_menu_block:main',
              'label' => '',
              'label_display' => 'visible',
              'provider' => 'system',
              'level' => 1,
              'depth' => 0,
              'expand_all_items' => FALSE,
              'region' => 'first',
              'weight' => 0,
              'uuid' => '23df847b-2809-4db0-813a-59547c9e41fa',
            ],
            '51dc2014-a67a-4275-b394-4ffbed73da16' => [
              'id' => 'user_login_block',
              'label' => '',
              'label_display' => 'visible',
              'provider' => 'user',
              'region' => 'third',
              'weight' => 0,
              'uuid' => '51dc2014-a67a-4275-b394-4ffbed73da16',
            ],
            'f7702500-a46c-4823-a4ca-3e1833b30459' => [
              'id' => 'system_menu_block:account',
              'label' => '',
              'label_display' => 'visible',
              'provider' => 'system',
              'level' => 1,
              'depth' => 0,
              'expand_all_items' => FALSE,
              'region' => 'third',
              'weight' => 1,
              'uuid' => 'f7702500-a46c-4823-a4ca-3e1833b30459',
            ],
            '8ea0fe88-c68b-4849-ae9f-2ba88f642e8e' => [
              'id' => 'page_manager_migration:node:node:1:8',
              'label' => '',
              'label_display' => 'visible',
              'provider' => 'core',
              'region' => 'second',
              'weight' => 0,
              'uuid' => '8ea0fe88-c68b-4849-ae9f-2ba88f642e8e',
            ],
          ],
          'page_title' => '',
          'layout' => 'layout_threecol_33_34_33',
          'layout_settings' => [],
          'storage_type' => 'page_manager',
          'storage_id' => 'page_simple_landing_page__panel_context_dabd57ca-648c-4561-be3a-1c6a379910a3',
        ],
        'page' => 'simple_landing_page',
        'weight' => 0,
        'selection_criteria' => [],
        'selection_logic' => 'and',
        'static_context' => [],
      ],
      array_diff_key(
        PageVariant::load('page_simple_landing_page__panel_context_dabd57ca-648c-4561-be3a-1c6a379910a3')->toArray(),
        ['dependencies' => 1]
      )
    );
  }

  /**
   * Checks the test page which consists of http_status_code variants only.
   */
  protected function assertTestPage1() {
    // Check the page.
    $this->assertEquals(
      [
        'langcode' => 'en',
        'status' => TRUE,
        'id' => 'test_page_1',
        'label' => 'Test page 1',
        'description' => '',
        'use_admin_theme' => FALSE,
        'path' => '/test/page/1',
        'access_logic' => 'and',
        'access_conditions' => [],
        'parameters' => [],
      ],
      array_diff_key(
        Page::load('test_page_1')->toArray(),
        ['uuid' => 1, 'dependencies' => 1]
      )
    );

    // Check the 403 variant.
    $this->assertEquals(
      [
        'uuid' => 'a714b675-418e-4987-bafd-93eb7007305c',
        'langcode' => 'en',
        'status' => TRUE,
        'id' => 'page_test_page_1__http_response_a714b675-418e-4987-bafd-93eb7007305c',
        'label' => 'HTTP 403 for administrators',
        'variant' => 'http_status_code',
        'variant_settings' => [
          'id' => 'http_status_code',
          'label' => 'HTTP 403 for administrators',
          'weight' => -2,
          'uuid' => 'a714b675-418e-4987-bafd-93eb7007305c',
          'status_code' => 404,
          'redirect_location' => '',
        ],
        'page' => 'test_page_1',
        'weight' => -2,
        'selection_criteria' => [
          [
            'id' => 'user_role',
            'negate' => FALSE,
            'context_mapping' => [
              'user' => '@user.current_user_context:current_user',
            ],
            'roles' => ['admin' => 'admin'],
          ],
        ],
        'selection_logic' => 'and',
        'static_context' => [],
      ],
      array_diff_key(
        PageVariant::load('page_test_page_1__http_response_a714b675-418e-4987-bafd-93eb7007305c')->toArray(),
        ['dependencies' => 1]
      )
    );

    // Check the 404 variant.
    $this->assertEquals(
      [
        'uuid' => 'cf847802-66ee-4ab9-93fd-31f11af1ff01',
        'langcode' => 'en',
        'status' => TRUE,
        'id' => 'page_test_page_1__http_response_cf847802-66ee-4ab9-93fd-31f11af1ff01',
        'label' => 'HTTP 404 for logged in',
        'variant' => 'http_status_code',
        'variant_settings' => [
          'id' => 'http_status_code',
          'label' => 'HTTP 404 for logged in',
          'weight' => -1,
          'uuid' => 'cf847802-66ee-4ab9-93fd-31f11af1ff01',
          'status_code' => 404,
          'redirect_location' => '',
        ],
        'page' => 'test_page_1',
        'weight' => -1,
        'selection_criteria' => [
          [
            'id' => 'user_role',
            'negate' => TRUE,
            'context_mapping' => [
              'user' => '@user.current_user_context:current_user',
            ],
            'roles' => ['anonymous' => 'anonymous'],
          ],
        ],
        'selection_logic' => 'and',
        'static_context' => [],
      ],
      array_diff_key(
        PageVariant::load('page_test_page_1__http_response_cf847802-66ee-4ab9-93fd-31f11af1ff01')->toArray(),
        ['dependencies' => 1]
      )
    );

    // Check the 410 (fallback) variant.
    $variant_410_array = PageVariant::load('page-test_page_1__http_410')->toArray();
    unset($variant_410_array['variant_settings']['uuid']);
    $this->assertEquals(
      [
        'langcode' => 'en',
        'status' => TRUE,
        'id' => 'page-test_page_1__http_410',
        'label' => 'HTTP 410 fallback',
        'variant' => 'http_status_code',
        'variant_settings' => [
          'id' => 'http_status_code',
          'label' => 'HTTP 410 fallback',
          'weight' => 0,
          'status_code' => 410,
          'redirect_location' => '',
        ],
        'page' => 'test_page_1',
        'weight' => 0,
        'selection_criteria' => [],
        'selection_logic' => 'and',
        'static_context' => [],
      ],
      array_diff_key(
        $variant_410_array,
        ['uuid' => 1, 'dependencies' => 1]
      )
    );
  }

}
