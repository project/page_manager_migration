# About Page Manager Migration

## Contents

 * [Introduction](#introduction)
 * [Requirements](#requirements)
 * [Installation](#installation)
 * [Configuration](#configuration)
 * [Usage](#usage)
 * [Maintainers](#maintainers)


## Introduction

Page Manager Migration is a proof-of-concept module which migrates Page Manager
pages and Page Manager & Panels page variants to the most recent Drupal core
version.

The development is in a very early phase, so please don't expect any magic!

Keep in mind that we can migrate only those PM and Panels configs which are
stored in the database. These _things_ are CTools object, so they might be
stored in code as well!


## Requirements

 * [Migrate Drupal UI][1] (included in Drupal core)
 * [Page Manager][2]: `4.0.0-beta6` at least
 * [Panels][3]: `4.6.0` at least
 * [Migrate Magician][4]: `1.6.0` at least


## Installation

Install Page Manager Migration as you would normally install a contributed
Drupal 8+ module (so: use Composer!).


## Configuration

This module does not have any configuration option.


## Usage

Short:
 * Enable [Migrate Drupal UI][1].
 * Configure your Drupal 7 source database.
 * Perform the upgrade!

Longer: Page Manager Migration module supports only [Migrate Drupal UI][1], or
any other [Migrate Drupal UI][1]-compatible tools which are building migration
dependency graph and are using that order for executing the discovered
migrations.


## Maintainers

 * Zoltán Horváth ([huzooka](https://www.drupal.org/u/huzooka))

---

This project has been sponsored by [Acquia][5].

[1]: https://drupal.org/docs/core-modules-and-themes/core-modules/migrate-drupal-ui-module
[2]: https://drupal.org/project/page_manager
[3]: https://drupal.org/project/panels
[4]: https://drupal.org/project/migmag
[5]: https://acquia.com
